<?php 

    // VENDOR_DIR = pasta vendor da sua instalação composer
    require 'vendor/autoload.php';

    error_reporting(E_ALL);
    ini_set('display_errors', 'On');

    require_once 'bootstrap.php';
    use NFePHP\DA\NFe\Danfe;
    use NFePHP\DA\Legacy\FilesFolders;

    $xml    = 'xmls/41171183188110000318550010000049831049388337-nfe.xml';
    $docxml = FilesFolders::readFile($xml);
    //$logo   = 'data://text/plain;base64,'. base64_encode(file_get_contents('images/logo.jpg'));
    
    try {
        $danfe = new Danfe($docxml, 'P', 'A4', $logo, 'I', '');
        $id    = $danfe->montaDANFE();
        $pdf   = $danfe->render();

        header('Content-Type: application/pdf');
        echo $pdf;
    } catch (InvalidArgumentException $e) {
        echo "Ocorreu um erro durante o processamento :" . $e->getMessage();
    }    